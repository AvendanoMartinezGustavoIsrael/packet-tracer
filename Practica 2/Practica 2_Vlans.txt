//10 / 07 / 2019
//REDES (VLANS)
######################################################
#####################Switch 1#########################
######################################################
enable
configure terminal

interface vlan 10
ip address 192.168.10.254 255.255.255.0
no shutdown
exit
interface vlan 20
ip address 192.168.20.254 255.255.255.0
no shutdown
exit
interface vlan 30
ip address 192.168.30.254 255.255.255.0
no shutdown
exit
interface fastEthernet 0/1
vlan 10
name Primaria
exit
interface FastEthernet 0/9
vlan 20
name Secundaria
exit
interface FastEthernet 0/17
vlan 30
name Bachillerato
exit

interface FastEthernet 0/1
switchport mode access
switchport access vlan 10
exit
interface FastEthernet 0/9
switchport mode access
switchport access vlan 20
exit
interface FastEthernet 0/17
switchport mode access
switchport access vlan 30
exit
interface GigabitEthernet 0/1
switchport mode trunk
exit
write
show vlan br 

######################################################
#####################Switch 2#########################
######################################################

enable
configure terminal
interface vlan 10
ip address 192.168.10.253 255.255.255.0
no shutdown
exit
interface vlan 20
ip address 192.168.20.253 255.255.255.0
no shutdown
exit
interface vlan 30
ip address 192.168.30.253 255.255.255.0
no shutdown
exit
interface fastEthernet 0/1
vlan 10
name Primaria
exit
interface FastEthernet 0/9
vlan 20
name Secundaria
exit
interface FastEthernet 0/17
vlan 30
name Bachillerato
exit

interface FastEthernet 0/1
switchport mode access
switchport access vlan 10
exit
interface FastEthernet 0/9
switchport mode access
switchport access vlan 20
exit
interface FastEthernet 0/17
switchport mode access
switchport access vlan 30
exit
interface GigabitEthernet 0/1
switchport mode trunk
exit
exit
write
show vlan br


interface GigabitEthernet 0/2
switchport mode trunk

######################################################
#####################Switch 2#########################
######################################################

enable
configure terminal
interface vlan 10
ip address 192.168.10.252 255.255.255.0
no shutdown
exit
interface vlan 20
ip address 192.168.20.252 255.255.255.0
no shutdown
exit
interface vlan 30
ip address 192.168.30.252 255.255.255.0
no shutdown
exit
interface fastEthernet 0/1
vlan 10
name Primaria
exit
interface FastEthernet 0/9
vlan 20
name Secundaria
exit
interface FastEthernet 0/17
vlan 30
name Bachillerato
exit

interface FastEthernet 0/1
switchport mode access
switchport access vlan 10
exit
interface FastEthernet 0/9
switchport mode access
switchport access vlan 20
exit
interface FastEthernet 0/17
switchport mode access
switchport access vlan 30
exit
interface GigabitEthernet 0/2
switchport mode trunk
exit
exit
write
show vlan br
